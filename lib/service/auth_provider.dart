import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'auth_service.dart';

final authServiceProvider = Provider<AuthenticationService>((ref) {
  return AuthenticationService(ref.read);
});

// ignore: top_level_function_literal_block
final authStateProvider = StreamProvider((ref) {
  return ref.watch(authServiceProvider).authStateChanges;
});

final firebaseUserFutureProvider = FutureProvider<User?>(
  (ref) async {
    final firebaseUser = await ref.watch(authStateProvider.last);
    return firebaseUser;
  },
);
