import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/model/pet.dart';

final petRepositoryProvider =
    Provider.autoDispose<PetRepository>((ref) => PetRepository(ref.read));

class PetRepository {
  final Reader read;
  PetRepository(this.read);

  Future<List<Pet>> fetchPets() async {
    try {
      var response = await Dio().get(
          "https://data.coa.gov.tw/Service/OpenData/TransService.aspx?UnitId=QcbUEzN6E6DL");
      List<Pet> pets = (response.data as List)
          .map((jsonObject) => Pet.fromJson(jsonObject))
          .toList();
      return pets;
    } catch (e) {
      throw Exception('error');
    }
  }
}
