import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/model/filter_model.dart';
import 'package:pets/model/pet.dart';
import 'package:pets/model/pet_model.dart';
import 'package:pets/providers/filter_provider.dart';
import 'package:pets/screen/widgets/pet_card.dart';

class Adoption extends StatefulWidget {
  @override
  _AdoptionState createState() => _AdoptionState();
}

class _AdoptionState extends State<Adoption> {
  PetUtil petModel = PetUtil();
  String dropdownValue = '篩選條件';

  KindFilter getKindFilter(String? value) {
    switch (value) {
      case '全部':
        return KindFilter.all;
      case '貓':
        return KindFilter.cat;
      case '狗':
        return KindFilter.dog;
      case '其他':
        return KindFilter.other;
      default:
        return KindFilter.all;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        AsyncValue<List<Pet>?> petsAsyncValue = watch(filteredListProvider);
        return petsAsyncValue.when(
          data: (pets) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10.0,
                ),
                SizedBox(
                  height: 50,
                  child: DropdownButton<String>(
                    iconEnabledColor: Color(0xff6200ee),
                    value: dropdownValue,
                    icon: const Icon(Icons.arrow_drop_down),
                    iconSize: 24,
                    // elevation: 16,
                    style: TextStyle(color: Color(0xff6200ee)),
                    underline: Container(
                      color: Colors.transparent,
                      // height: 1,
                      // color: Color(0xff6200ee),
                    ),
                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownValue = newValue!;
                        context.read(kindFilterProvider).state =
                            getKindFilter(newValue);
                      });
                    },
                    items: <String>['篩選條件', '全部', '貓', '狗', '其他']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: pets!.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 4, horizontal: 16),
                        child: PetCard(
                          pet: pets[index],
                        ),
                      );
                    },
                  ),
                ),
              ],
            );
          },
          loading: () {
            return Container(
              height: 280,
              width: 344,
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black26),
                ),
              ),
            );
          },
          error: (e, s) => Text(e.toString()),
        );
      },
    );
  }
}
