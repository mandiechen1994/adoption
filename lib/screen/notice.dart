import 'package:flutter/material.dart';

class Notice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: double.infinity,
                  height: 150,
                  child: Image(
                    height: 150,
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        'https://i1.wp.com/blog.gowifi.com.tw/wp-content/uploads/2020/05/adorable-3344414_640.jpg?w=640&ssl=1'),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  child: ListTile(
                    title: Text(
                      '有足夠的時間與耐心陪伴、照顧毛小孩嗎？',
                      style: TextStyle(fontSize: 15),
                    ),
                    subtitle: Text(
                      '不只狗狗們需要每天出去跑跑散步，貓咪也是非常需要主人的陪伴的哦！試想如果你是毛孩，每天都自己一個人，主人在家的時間也就短短 4-6 小時卻也不願意跟你玩，你會怎麼樣？因此如果不能每天花時間與毛孩相處，或者經常出遠門的人可能就不太適合養寵物哦！'
                      '另外毛孩們也不像人類可以「進行雙方面的溝通」，若當牠們如果出現像是吠叫、咬壞東西、隨地大小便等較不可控的行為時，你是否有願意包容、理解牠們為何會出現這些行為，並有耐心地教導牠們改善呢？',
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  child: ListTile(
                    title: Text(
                      '經濟能力負擔得起毛孩所有開銷嗎？',
                      style: TextStyle(fontSize: 15),
                    ),
                    subtitle: Text(
                      '不管是養貓還是養狗，飼料、玩具、外出籠等物品都是固定開銷，醫療費用像是結紮、疫苗、健康檢查等也是必要的支出！如果說寵物生病或年老身體變差，門診/急診/住院或手術等費用，有時候動輒也要好幾千甚至近萬元。'
                      '而且現在越來越多人因為想要讓寶貝毛孩們的保障更加全面，也會幫寵物們投保寵物險以應付臨時的龐大支出（從寵物突發疾病、飼主因病須將寵物寄宿、到協尋等皆有理賠），這些花費零零總總加起來一年大約 5-6 萬，甚至可能更高。因此在養寵物前，請先想想自己目前否負擔得起這些開銷，如果沒有還是先養好自己，等到有一定的經濟能力後再養寵物吧！',
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  child: ListTile(
                    title: Text(
                      '目前的居住環境是否適合牠們？',
                      style: TextStyle(fontSize: 15),
                    ),
                    subtitle: Text(
                      '其實總體來說，養寵物除了做足功課之外，最重要的就是有沒有做好要對牠們的一生負責的準備。對人類來說，寵物的陪伴或許只是幾十年的時間，對毛孩來說卻是一輩子的事情，而牠們能依靠、信任的也只有我們。因此千萬不要因為一時衝動或好玩將牠們帶回家，卻吝嗇花時間給予陪伴甚至遺棄牠們，請將毛孩當作家人一樣照顧，並扛起對生命負責的責任。',
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  child: ListTile(
                    title: Text(
                      '以上資訊希望可以幫助每位想養狗狗、貓貓的人更了解飼養寵物的責任與義務，飼養前請務必三思，也希望大家盡量以領養代替購買，讓浪浪們都可以找到幸福的家噢！',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
