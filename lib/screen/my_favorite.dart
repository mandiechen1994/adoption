import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/model/pet.dart';
import 'package:pets/model/pet_model.dart';
import 'package:pets/providers/pet_provider.dart';
import 'package:pets/screen/widgets/pet_card.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyFavorite extends StatefulWidget {
  // final Future<List<Pet>> futureData;

  @override
  _MyFavoriteState createState() => _MyFavoriteState();
}

class _MyFavoriteState extends State<MyFavorite> {
  PetUtil petModel = PetUtil();
  late List<String> favoriteList;
  late SharedPreferences prefs;

  Widget getChip(String result) {
    if (result != null) {
      return Padding(
          padding: EdgeInsets.only(left: 8), child: Chip(label: Text(result)));
    } else {
      return Container();
    }
  }

  // getSkelotnView() {
  //   double width = MediaQuery.of(context).size.width;
  //   double height = (width / 4) * 3;
  //   return ListView.builder(
  //       padding: EdgeInsets.fromLTRB(8.0, 32.0, 8.0, 32.0),
  //       itemCount: 8,
  //       itemBuilder: (BuildContext context, int index) {
  //         return Card(
  //           margin: EdgeInsets.all(8),
  //           child: InkWell(
  //             onTap: () {},
  //             child: Column(
  //               children: <Widget>[
  //                 Container(
  //                   height: height,
  //                   width: double.infinity,
  //                   color: Colors.black26,
  //                   child: Center(
  //                     child: CircularProgressIndicator(
  //                       valueColor:
  //                           AlwaysStoppedAnimation<Color>(Colors.black26),
  //                     ),
  //                   ),
  //                 ),
  //                 ListTile(
  //                   title: Row(
  //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                     children: <Widget>[
  //                       ClipRRect(
  //                         borderRadius: BorderRadius.circular(4.0),
  //                         child: Container(
  //                           color: Colors.black26,
  //                           width: 80,
  //                           height: 24,
  //                         ),
  //                       ),
  //                       ClipRRect(
  //                         borderRadius: BorderRadius.circular(4.0),
  //                         child: Container(
  //                           color: Colors.black26,
  //                           width: 80,
  //                           height: 24,
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                   trailing: ClipRRect(
  //                     borderRadius: BorderRadius.circular(4.0),
  //                     child: Container(
  //                       color: Colors.black26,
  //                       width: 40,
  //                       height: 24,
  //                     ),
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        AsyncValue<List<Pet>> petsAsyncValue =
            watch(filteredFavoritePetListProvider);
        return petsAsyncValue.when(
          data: (pets) {
            if (pets.length == 0) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.pets,
                    size: 60,
                    color: Colors.black38,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    '目前沒有加入最愛的毛小孩',
                    style: TextStyle(color: Colors.black54, fontSize: 20),
                  )
                ],
              );
            }
            return ListView.builder(
              itemCount: pets.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
                  child: PetCard(
                    pet: pets[index],
                  ),
                );
              },
            );
          },
          loading: () {
            return Container(
              height: 280,
              width: 344,
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black26),
                ),
              ),
            );
          },
          error: (e, s) => Text(e.toString()),
        );
      },
    );
  }
}
