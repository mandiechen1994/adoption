import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
// import 'package:pets/screen/backdrop.dart';
import 'package:pets/screen/notice.dart';

import 'adoption.dart';
import 'my_favorite.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('寵物認養平台'),
          // leading: IconButton(
          //   icon: Icon(
          //     Icons.menu,
          //     color: Colors.white,
          //   ),
          //   onPressed: () {
          //     Navigator.push(
          //         context, MaterialPageRoute(builder: (context) => Backdrop()));
          //   },
          // ),
          actions: [
            PopupMenuButton(
              icon: Icon(Icons.more_vert),
              itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                PopupMenuItem(
                  child: TextButton(
                    onPressed: () {
                      _auth.signOut();
                    },
                    child: Text('登出'),
                  ),
                ),
              ],
            ),
          ],
          bottom: const TabBar(
            tabs: [
              Tab(text: '認養須知'),
              Tab(text: '認領養'),
              Tab(text: '最愛'),
            ],
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(0),
          child: TabBarView(
            children: [
              Notice(),
              Adoption(),
              MyFavorite(),
            ],
          ),
        ),
      ),
    );
  }
}
