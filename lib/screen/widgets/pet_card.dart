import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/model/pet.dart';
import 'package:pets/model/pet_model.dart';
import 'package:pets/providers/favorites_provider.dart';
import 'package:share/share.dart';

import '../pet_detail.dart';

class PetCard extends StatefulWidget {
  final Pet pet;

  const PetCard({Key? key, required this.pet}) : super(key: key);

  @override
  _PetCardState createState() => _PetCardState();
}

class _PetCardState extends State<PetCard> {
  late Pet pet;

  @override
  void initState() {
    super.initState();
    pet = widget.pet;
  }

  Widget getFavoriteIcon(bool isFavorite) {
    if (isFavorite) {
      return Icon(
        Icons.favorite,
        color: Colors.red[500],
      );
    } else {
      return Icon(
        Icons.favorite_border,
        color: Colors.black45,
      );
    }
  }

  void updateFavorite(BuildContext context, int animalId, bool isFavorite) {
    if (isFavorite) {
      context
          .read(favoritesStateNotifierProvider.notifier)
          .addFavorite(animalId.toString());
    } else {
      context
          .read(favoritesStateNotifierProvider.notifier)
          .removeFavorite(animalId.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        List<String> favorites = watch(favoritesStateNotifierProvider);
        bool isFavorite = favorites.contains(pet.animalId.toString());
        return Card(
          clipBehavior: Clip.antiAlias,
          child: InkWell(
            child: Column(
              children: [
                SizedBox(
                  width: double.infinity,
                  height: 280,
                  child: CachedNetworkImage(
                    imageUrl: pet.albumFile!,
                    width: 120,
                    height: 120,
                    fit: BoxFit.cover,
                    placeholder: (context, url) {
                      return Container(
                        height: 280,
                        width: 344,
                        child: Center(
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.black26),
                          ),
                        ),
                      );
                    },
                    errorWidget: (context, url, error) => Icon(Icons.pets),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text(
                        context
                            .read(petUtilProvider)
                            .getLocale(pet.animalAreaPkid!),
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ),
                    ButtonBar(
                      alignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          icon: getFavoriteIcon(isFavorite),
                          onPressed: () {
                            int? id = pet.animalId;
                            if (id != null) {
                              updateFavorite(context, id, !isFavorite);
                            }
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.share, color: Colors.black45),
                          onPressed: () {
                            String? shelterName = pet.shelterName;
                            String? albumFile = pet.shelterName;

                            if (shelterName != null && albumFile != null) {
                              Share.share(shelterName + '' + albumFile);
                            }
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (BuildContext context) {
                    return PetIntro(pet: pet);
                  },
                  fullscreenDialog: true,
                ),
              );
            },
          ),
        );
      },
    );
  }
}
