import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/model/exceptions.dart';
import 'package:pets/service/auth_provider.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController _pwdController = new TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    emailController.dispose();
    _pwdController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('註冊'),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 180,
                child: TextFormField(
                    controller: emailController,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: "Email",
                      prefixIcon: Icon(Icons.person),
                      hintText: '輸入email',
                    ),
                    // ignore: missing_return
                    validator: (email) {
                      if (email != null) {
                        return email.trim().length > 0 ? null : "email不可為空";
                      }
                    }),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                width: 180,
                child: TextFormField(
                    controller: _pwdController,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "Password",
                      prefixIcon: Icon(Icons.admin_panel_settings_sharp),
                      // border: OutlineInputBorder(),
                      hintText: '輸入密碼',
                    ),
                    // ignore: missing_return
                    validator: (password) {
                      if (password != null) {
                        return password.trim().length > 5 ? null : "密碼不可少於6位";
                      }
                    }),
              ),
              SizedBox(
                height: 25,
              ),
              OutlinedButton(
                onPressed: () async {
                  String email = emailController.text;
                  String password = _pwdController.text;
                  if (_formKey.currentState!.validate()) {
                    //驗證通過提交數據
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Processing Data')),
                    );
                  }
                  try {
                    await context
                        .read(authServiceProvider)
                        .registerUserByEmail(email, password);
                  } on AccountAlreadyExistsException catch (e) {
                    print(e);
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(e.cause)),
                    );
                  } on Exception catch (e) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(e.toString())),
                    );
                  }
                  Navigator.pop(context);
                  // on AccountAlreadyExistsException catch (e) {
                  //   ScaffoldMessenger.of(context).showSnackBar(
                  //     SnackBar(content: Text(e.cause)),
                  //   );
                  // }
                },
                child: Text('完成'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
