import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/model/pet_model.dart';

class Backdrop extends StatefulWidget {
  @override
  _BackdropState createState() => _BackdropState();
}

class _BackdropState extends State<Backdrop> {
  TextStyle titleStyle = TextStyle(color: Colors.white);
  PetUtil petModel = PetUtil();
  late List<int> areaPkidList;
  String dropdownValue = PetUtil().shelterMap[-1];

  @override
  void initState() {
    areaPkidList = List<int>.generate(22, (i) => i = i + 2);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        return Scaffold(
          backgroundColor: Color(0xff6200ee),
          appBar: AppBar(
            title: Text(
              '搜尋中 ...',
              style: TextStyle(fontSize: 20),
            ),
            actions: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(
                  child: ElevatedButton(
                    onPressed: () {},
                    child: Text(
                      '重設篩選',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
            // leading: IconButton(
            //   icon: AnimatedIcon(
            //     icon: AnimatedIcons.close_menu,progress: ,
            //   ), onPressed: () {  },
            // ),
          ),
          body: Consumer(builder: (context, appState, child) {
            return Column(
              children: [
                Container(
                  child: Divider(
                    indent: 8,
                    endIndent: 8,
                    color: Color(0xffbb86fc),
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            '選擇性別',
                            style: titleStyle,
                          ),
                          Wrap(
                            spacing: 8,
                            children: [
                              MyFilterChip(
                                label: petModel.getSex('F'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                              MyFilterChip(
                                label: petModel.getSex('M'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                            ],
                          ),
                          Divider(
                            color: Color(0xffbb86fc),
                          ),
                          Text(
                            '選擇類型',
                            style: titleStyle,
                          ),
                          Wrap(
                            spacing: 8,
                            children: [
                              MyFilterChip(
                                label: petModel.getKind('貓'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                              MyFilterChip(
                                label: petModel.getKind('狗'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                              MyFilterChip(
                                label: petModel.getKind('其他'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                            ],
                          ),
                          Divider(
                            color: Color(0xffbb86fc),
                          ),
                          Text(
                            '選擇體型',
                            style: titleStyle,
                          ),
                          Wrap(
                            spacing: 8,
                            children: [
                              MyFilterChip(
                                label: petModel.getBody('SMALL'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                              MyFilterChip(
                                label: petModel.getBody('MEDIUM'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                              MyFilterChip(
                                label: petModel.getBody('BIG'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                            ],
                          ),
                          Divider(
                            color: Color(0xffbb86fc),
                          ),
                          Text(
                            '選擇住所',
                            style: titleStyle,
                          ),
                          Theme(
                            data: Theme.of(context).copyWith(
                                canvasColor:
                                    ColorScheme.light().primaryVariant),
                            child: Container(
                              width: 60,
                              child: DropdownButton<String>(
                                iconEnabledColor: Colors.white,
                                value: dropdownValue,
                                hint: Text('選擇收容所'),
                                icon: Icon(
                                  Icons.keyboard_arrow_down,
                                ),
                                iconSize: 24,
                                style: TextStyle(color: Colors.white),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    dropdownValue = newValue!;
                                  });
                                },
                                items: <String>[
                                  petModel.shelterMap[-1],
                                  petModel.shelterMap[48],
                                  petModel.shelterMap[49],
                                  petModel.shelterMap[50],
                                  petModel.shelterMap[51],
                                  petModel.shelterMap[53],
                                  petModel.shelterMap[55],
                                  petModel.shelterMap[56],
                                  petModel.shelterMap[58],
                                  petModel.shelterMap[59],
                                  petModel.shelterMap[60],
                                  petModel.shelterMap[61],
                                  petModel.shelterMap[62],
                                  petModel.shelterMap[63],
                                  petModel.shelterMap[67],
                                  petModel.shelterMap[68],
                                  petModel.shelterMap[69],
                                  petModel.shelterMap[70],
                                  petModel.shelterMap[71],
                                  petModel.shelterMap[72],
                                  petModel.shelterMap[73],
                                  petModel.shelterMap[74],
                                  petModel.shelterMap[75],
                                  petModel.shelterMap[76],
                                  petModel.shelterMap[77],
                                  petModel.shelterMap[78],
                                  petModel.shelterMap[79],
                                  petModel.shelterMap[80],
                                  petModel.shelterMap[81],
                                  petModel.shelterMap[82],
                                  petModel.shelterMap[83],
                                  petModel.shelterMap[89],
                                  petModel.shelterMap[92],
                                  petModel.shelterMap[96],
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                          Divider(
                            color: Color(0xffbb86fc),
                          ),
                          Text(
                            '所在縣市',
                            style: titleStyle,
                          ),
                          Wrap(
                            spacing: 8,
                            children: areaPkidList.map<Widget>((s) {
                              return MyFilterChip(
                                label: petModel.getLocale(s),
                                // selected: null,
                                onSelected: (bool value) {},
                              );
                            }).toList(),
                          ),
                          Divider(
                            color: Color(0xffbb86fc),
                          ),
                          Text(
                            '年紀',
                            style: titleStyle,
                          ),
                          Wrap(
                            spacing: 8,
                            children: [
                              MyFilterChip(
                                label: petModel.getAge('CHILD'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                              MyFilterChip(
                                label: petModel.getAge('ADULT'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                            ],
                          ),
                          Divider(
                            color: Color(0xffbb86fc),
                          ),
                          Text(
                            '是否絕育',
                            style: titleStyle,
                          ),
                          Wrap(
                            spacing: 8,
                            children: [
                              MyFilterChip(
                                label: petModel.getSterilization('T'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                              MyFilterChip(
                                label: petModel.getSterilization('F'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                              MyFilterChip(
                                label: petModel.getSterilization('N'),
                                // selected: null,
                                onSelected: (bool value) {},
                              ),
                            ],
                          ),
                          Divider(
                            color: Color(0xffbb86fc),
                          ),
                          SizedBox(
                            height: 50,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            );
          }),
        );
      },
    );
  }
}

class MyFilterChip extends StatelessWidget {
  final String? label;
  // final bool selected;
  final ValueChanged<bool> onSelected;

  MyFilterChip({required this.label, required this.onSelected});

  @override
  Widget build(BuildContext context) {
    return FilterChip(
      // selected: selected,
      label: Text(
        label!,
        style: TextStyle(color: Colors.white),
      ),
      onSelected: onSelected,
      backgroundColor: Color(0xffbe9eff),
      selectedColor: Color(0xffbb86fc),
      checkmarkColor: Colors.white,
    );
  }
}
