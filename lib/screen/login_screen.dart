import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/screen/register_screen.dart';
import 'package:pets/service/auth_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isChecked = false;
  TextEditingController emailController = new TextEditingController();
  TextEditingController _pwdController = new TextEditingController();
  TextEditingController accountTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
    init();
  }

  init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      isChecked = prefs.getBool('remember_account') ?? false;
      String accountText = prefs.getString('account') ?? '';
      emailController.text = accountText;
    });
  }

  onRememberButtonClick(bool isSave) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (isSave) {
      String accountText = emailController.text;
      await prefs.setBool('remember_account', isSave);
      await prefs.setString('account', accountText);
    } else {
      await prefs.setBool('remember_account', isSave);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('登入'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 180,
              child: TextFormField(
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: "Email",
                  // prefixIcon: Icon(Icons.person),
                  hintText: '輸入email',
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: 180,
              child: TextFormField(
                controller: _pwdController,
                obscureText: true,
                decoration: InputDecoration(
                  labelText: "Password",
                  // prefixIcon: Icon(Icons.admin_panel_settings_sharp),
                  hintText: '輸入密碼',
                ),
              ),
            ),
            SizedBox(
              height: 8.0,
            ),
            Container(
              width: 200,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text('記住帳號'),
                  Checkbox(
                    value: isChecked,
                    activeColor: Color(0xff6200ee),
                    onChanged: (value) {
                      setState(
                        () {
                          isChecked = value ?? false;
                          onRememberButtonClick(isChecked);
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            ElevatedButton(
                onPressed: () {
                  String email = emailController.text;
                  String password = _pwdController.text;
                  context
                      .read(authServiceProvider)
                      .signInWithEmailAndPassword(email, password);
                },
                child: Text('登入')),
            OutlinedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => RegisterScreen(),
                  ),
                );
              },
              child: Text('註冊'),
            ),
          ],
        ),
      ),
    );
  }
}
