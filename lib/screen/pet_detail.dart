import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:pets/model/pet.dart';
import 'package:pets/model/pet_model.dart';
import 'package:pets/providers/favorites_provider.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class PetIntro extends StatefulWidget {
  final Pet pet;
  PetIntro({required this.pet});

  @override
  _PetIntroState createState() => _PetIntroState();
}

class _PetIntroState extends State<PetIntro> {
  PetUtil petModel = PetUtil();

  Widget getFavoriteIcon(bool isFavorite) {
    if (isFavorite) {
      return Icon(
        Icons.favorite,
        color: Colors.red[500],
      );
    } else {
      return Icon(
        Icons.favorite_border,
        color: Colors.black45,
      );
    }
  }

  void updateFavorite(BuildContext context, int animalId, bool isFavorite) {
    if (isFavorite) {
      context
          .read(favoritesStateNotifierProvider.notifier)
          .addFavorite(animalId.toString());
    } else {
      context
          .read(favoritesStateNotifierProvider.notifier)
          .removeFavorite(animalId.toString());
    }
  }

  Widget getChip(String? result) {
    if (result != null) {
      return Padding(
        padding: EdgeInsets.only(left: 8.0),
        child: Chip(
          labelStyle: TextStyle(color: Colors.black54),
          label: Text(result),
        ),
      );
    } else {
      return Container(
        width: 0,
        height: 0,
      );
    }
  }

  Widget getRemarkListTile() {
    if (widget.pet.animalRemark == '') {
      return Container();
    } else {
      return ListTile(
        dense: true,
        leading: Icon(
          Icons.rate_review,
        ),
        subtitle: Text(widget.pet.animalRemark ?? ""),
        title: Text(
          '備註',
          style: TextStyle(fontSize: 15),
        ),
      );
    }
  }

  _launchCaller(String? tel) async {
    if (tel == null) return;
    String url = "tel:$tel";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      List<String> favorites = watch(favoritesStateNotifierProvider);
      bool isFavorite = favorites.contains(widget.pet.animalId.toString());
      return Scaffold(
        appBar: new AppBar(
          title: const Text('關於我'),
          actions: [
            IconButton(
              icon: getFavoriteIcon(isFavorite),
              color: Colors.white,
              onPressed: () {
                int? id = widget.pet.animalId;
                if (id != null) {
                  updateFavorite(context, id, !isFavorite);
                }
              },
            ),
            IconButton(
              icon: Icon(Icons.share),
              color: Colors.white,
              onPressed: () {
                Share.share(
                    widget.pet.shelterName! + ' ' + widget.pet.albumFile!);
              },
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Column(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(4.0),
                      topLeft: Radius.circular(4.0)),
                  child: CachedNetworkImage(
                    imageUrl: widget.pet.albumFile!,
                    fit: BoxFit.cover,
                    width: double.infinity,
                    height: 280.0,
                    placeholder: (context, url) => Container(
                      child: CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) => Container(
                      color: Colors.black54,
                      height: 160,
                      width: double.infinity,
                      child: Icon(
                        Icons.pets,
                        size: 60,
                        color: Colors.black38,
                      ),
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 48.0,
                  child: ElevatedButton.icon(
                    icon: Icon(
                      Icons.call,
                      color: Colors.white,
                    ),
                    label: Text(
                      '聯絡收容中心',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    onPressed: () {
                      _launchCaller(widget.pet.shelterName);
                    },
                  ),
                ),
                Divider(),
                ListTile(
                  leading: Icon(
                    Icons.place,
                    size: 36,
                  ),
                  subtitle:
                      Text(petModel.getCurrentPlace(widget.pet.animalPlace)),
                  title: Text(
                    '所在地',
                    style: TextStyle(fontSize: 15),
                  ),
                  onTap: () {
                    if (widget.pet.shelterAddress != null &&
                        widget.pet.shelterAddress!.isNotEmpty) {
                      MapsLauncher.launchQuery(widget.pet.shelterAddress!);
                    }
                  },
                ),
                Divider(),
                ListTile(
                  leading: Icon(
                    Icons.local_offer,
                  ),
                  title: Container(
                    child: Wrap(
                      children: <Widget>[
                        getChip(petModel.getKind(widget.pet.animalKind)),
                        getChip(petModel.getSex(widget.pet.animalSex)),
                        getChip(petModel.getBody(widget.pet.animalBodytype)),
                        getChip(petModel.getColour(widget.pet.animalColour)),
                        getChip(petModel.getAge(widget.pet.animalAge)),
                        getChip(petModel
                            .getSterilization(widget.pet.animalSterilization)),
                      ],
                    ),
                  ),
                ),
                getRemarkListTile(),
                ListTile(
                  leading: Icon(
                    Icons.not_listed_location,
                  ),
                  // subtitle: Text(animalModel
                  //     .getFoundPlace(widget.animalData.animal_foundplace)),
                  title: Text(
                    '尋獲地',
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                Divider(),
                DetailInfoTile(
                  title: '資料建立時間:',
                  text: widget.pet.animalCreatetime ?? "",
                ),
                DetailInfoTile(
                  title: '最後更新時間:',
                  text: widget.pet.animalUpdate ?? "",
                ),
                DetailInfoTile(
                  title: '開放認養時間:',
                  text: widget.pet.animalOpendate ?? "",
                ),
                DetailInfoTile(
                  title: '區域編號:',
                  text: widget.pet.animalId.toString(),
                ),
                DetailInfoTile(
                  title: '流水編號:',
                  text: widget.pet.animalSubid.toString(),
                ),
                SizedBox(
                  height: 16,
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}

class DetailInfoTile extends StatelessWidget {
  final String title;
  final String text;
  DetailInfoTile({required this.title, required this.text});
  final TextStyle style = TextStyle(color: Colors.black54, fontSize: 11);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        SizedBox(width: 24),
        Text(title, style: style),
        Text(
          text,
          style: style,
        )
      ],
    );
  }
}
