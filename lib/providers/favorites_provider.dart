import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/providers/local_db_provider.dart';

// final favoritesProvider = StateProvider.autoDispose<List<String>>((ref) => []);

final favoritesStateNotifierProvider =
    StateNotifierProvider<FavoritesNotifier, List<String>>(
        (ref) => FavoritesNotifier(ref.read));

class FavoritesNotifier extends StateNotifier<List<String>> {
  final Reader read;

  FavoritesNotifier(this.read) : super([]) {
    init();
  }

  init() async {
    List<String> favorites =
        await read(sharedPreferencesClient).getFavoritesFromDb();
    state = favorites;
  }

  void addFavorite(String animalId) {
    state = [...state, animalId];
    read(sharedPreferencesClient).setFavoritesFromDb(state);
  }

  void removeFavorite(String removeId) {
    //先移除再重新塞回去
    List<String> temp = state;

    if (temp.contains(removeId)) {
      int index = temp.indexOf(removeId);
      temp.removeAt(index);
      state = temp;
      read(sharedPreferencesClient).setFavoritesFromDb(state);
    }

    // state = state.where((id) => id != removeId).toList();//參考
  }
}
