import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/model/pet.dart';
import 'package:pets/repositories/pet_repository.dart';

import 'favorites_provider.dart';

final petListFutureProvider = FutureProvider<List<Pet>>((ref) async {
  List<Pet> pets = await ref.read(petRepositoryProvider).fetchPets();
  return pets;
});

final filteredFavoritePetListProvider = FutureProvider<List<Pet>>((ref) async {
  //過濾
  final allPet = await ref.watch(petListFutureProvider.future);
  final favoriteList = ref.watch(favoritesStateNotifierProvider);
  return allPet
      .where((pet) => favoriteList.contains(pet.animalId.toString()))
      .toList();
});
