import 'package:flutter_riverpod/flutter_riverpod.dart';

final kindFilterProvider = StateProvider((ref) => KindFilter.all);
final sexFilterProvider = StateProvider((ref) => SexFilter.all);
final bodyFilterProvider = StateProvider((ref) => BodyFilter.all);

enum KindFilter {
  all,
  dog,
  cat,
  other,
}

enum SexFilter {
  all,
  male,
  female,
  other,
}

enum BodyFilter {
  all,
  small,
  medium,
  big,
}
