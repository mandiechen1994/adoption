import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sharedPreferencesClient = Provider((ref) => SharedPreferencesClient());

class SharedPreferencesClient {
  Future<List<String>> getFavoritesFromDb() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String> favorList = prefs.getStringList(_getFavoriteKey()) ?? [];
    return favorList;
  }

  Future<void> setFavoritesFromDb(List<String> favorList) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList(_getFavoriteKey(), favorList);
  }

  String _getFavoriteKey() {
    if (FirebaseAuth.instance.currentUser == null) throw Exception('No User');
    String uid = FirebaseAuth.instance.currentUser!.uid;
    return uid + "_favorites";
  }
}
