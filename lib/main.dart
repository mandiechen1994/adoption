import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/service/auth_provider.dart';
import 'package:pets/screen/home_screen.dart';
import 'package:pets/screen/login_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized(); //確認基本初始化完成
  await Firebase.initializeApp(); //FireBase初始化
  runApp(
    ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Adoption App',
      theme: ThemeData.from(
        colorScheme: const ColorScheme.light(),
      ),
      home: Root(),
    );
  }
}

//製監聽firebaseUser登入狀態 → 導入頁面。
class Root extends StatefulWidget {
  @override
  _RootState createState() => _RootState();
}

class _RootState extends State<Root> {
  @override
  Widget build(BuildContext context) {
    //讀取window尺寸
    double width = MediaQuery.of(context).size.width;
    double height = (width / 4) * 3;
    return Consumer(
      builder: (context, watch, child) {
        AsyncValue fireBaseUser = watch(firebaseUserFutureProvider);
        return fireBaseUser.when(
          data: (fireBaseUser) {
            if (fireBaseUser != null) {
              print(fireBaseUser);
              return HomeScreen();
            } else {
              return LoginScreen();
            }
          },
          loading: () {
            return Container(
              height: height,
              width: width,
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.black26),
                ),
              ),
            );
          },
          error: (e, s) => Text('error'),
        );
      },
    );
  }
}
