import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pets/model/pet.dart';
import 'package:pets/providers/filter_provider.dart';
import 'package:pets/providers/pet_provider.dart';

final filteredListProvider = FutureProvider<List<Pet>?>((ref) async {
  final kindFilter = ref.watch(kindFilterProvider).state;
  // final sexFilter = ref.watch(sexFilterProvider);
  // final bodyFilter = ref.watch(bodyFilterProvider);
  final allPet = await ref.watch(petListFutureProvider.future);

  switch (kindFilter) {
    case KindFilter.dog:
      return allPet.where((element) => element.animalKind == '狗').toList();
    case KindFilter.cat:
      return allPet.where((element) => element.animalKind == '貓').toList();
    case KindFilter.other:
      return allPet.where((element) => element.animalKind == '其他').toList();
    case KindFilter.all:
      return allPet;
      break;
  }
});
