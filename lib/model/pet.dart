/// animal_id : 220913
/// animal_subid : "110090505"
/// animal_area_pkid : 2
/// animal_shelter_pkid : 49
/// animal_place : "臺北市動物之家"
/// animal_kind : "狗"
/// animal_sex : "F"
/// animal_bodytype : "SMALL"
/// animal_colour : "黑白色"
/// animal_age : ""
/// animal_sterilization : "F"
/// animal_bacterin : "F"
/// animal_foundplace : "忠義捷運站附近"
/// animal_title : ""
/// animal_status : "OPEN"
/// animal_remark : "D.P. 寵愛"
/// animal_caption : ""
/// animal_opendate : "2021-09-05"
/// animal_closeddate : "2999-12-31"
/// animal_update : "2021/09/05"
/// animal_createtime : "2021/09/05"
/// shelter_name : "臺北市動物之家"
/// album_file : ""
/// album_update : ""
/// cDate : "2021/09/05"
/// shelter_address : "臺北市內湖區潭美街852號"
/// shelter_tel : "02-87913254"

class Pet {
  int? animalId;
  String? animalSubid;
  int? animalAreaPkid;
  int? animalShelterPkid;
  String? animalPlace;
  String? animalKind;
  String? animalSex;
  String? animalBodytype;
  String? animalColour;
  String? animalAge;
  String? animalSterilization;
  String? animalBacterin;
  String? animalFoundplace;
  String? animalTitle;
  String? animalStatus;
  String? animalRemark;
  String? animalCaption;
  String? animalOpendate;
  String? animalCloseddate;
  String? animalUpdate;
  String? animalCreatetime;
  String? shelterName;
  String? albumFile;
  String? albumUpdate;
  String? cDate;
  String? shelterAddress;
  String? shelterTel;

  Pet(
      {this.animalId,
      this.animalSubid,
      this.animalAreaPkid,
      this.animalShelterPkid,
      this.animalPlace,
      this.animalKind,
      this.animalSex,
      this.animalBodytype,
      this.animalColour,
      this.animalAge,
      this.animalSterilization,
      this.animalBacterin,
      this.animalFoundplace,
      this.animalTitle,
      this.animalStatus,
      this.animalRemark,
      this.animalCaption,
      this.animalOpendate,
      this.animalCloseddate,
      this.animalUpdate,
      this.animalCreatetime,
      this.shelterName,
      this.albumFile,
      this.albumUpdate,
      this.cDate,
      this.shelterAddress,
      this.shelterTel});

  Pet.fromJson(dynamic json) {
    animalId = json["animal_id"];
    animalSubid = json["animal_subid"];
    animalAreaPkid = json["animal_area_pkid"];
    animalShelterPkid = json["animal_shelter_pkid"];
    animalPlace = json["animal_place"];
    animalKind = json["animal_kind"];
    animalSex = json["animal_sex"];
    animalBodytype = json["animal_bodytype"];
    animalColour = json["animal_colour"];
    animalAge = json["animal_age"];
    animalSterilization = json["animal_sterilization"];
    animalBacterin = json["animal_bacterin"];
    animalFoundplace = json["animal_foundplace"];
    animalTitle = json["animal_title"];
    animalStatus = json["animal_status"];
    animalRemark = json["animal_remark"];
    animalCaption = json["animal_caption"];
    animalOpendate = json["animal_opendate"];
    animalCloseddate = json["animal_closeddate"];
    animalUpdate = json["animal_update"];
    animalCreatetime = json["animal_createtime"];
    shelterName = json["shelter_name"];
    albumFile = json["album_file"];
    albumUpdate = json["album_update"];
    cDate = json["cDate"];
    shelterAddress = json["shelter_address"];
    shelterTel = json["shelter_tel"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["animal_id"] = animalId;
    map["animal_subid"] = animalSubid;
    map["animal_area_pkid"] = animalAreaPkid;
    map["animal_shelter_pkid"] = animalShelterPkid;
    map["animal_place"] = animalPlace;
    map["animal_kind"] = animalKind;
    map["animal_sex"] = animalSex;
    map["animal_bodytype"] = animalBodytype;
    map["animal_colour"] = animalColour;
    map["animal_age"] = animalAge;
    map["animal_sterilization"] = animalSterilization;
    map["animal_bacterin"] = animalBacterin;
    map["animal_foundplace"] = animalFoundplace;
    map["animal_title"] = animalTitle;
    map["animal_status"] = animalStatus;
    map["animal_remark"] = animalRemark;
    map["animal_caption"] = animalCaption;
    map["animal_opendate"] = animalOpendate;
    map["animal_closeddate"] = animalCloseddate;
    map["animal_update"] = animalUpdate;
    map["animal_createtime"] = animalCreatetime;
    map["shelter_name"] = shelterName;
    map["album_file"] = albumFile;
    map["album_update"] = albumUpdate;
    map["cDate"] = cDate;
    map["shelter_address"] = shelterAddress;
    map["shelter_tel"] = shelterTel;
    return map;
  }
}
