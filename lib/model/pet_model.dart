import 'package:flutter_riverpod/flutter_riverpod.dart';

final petUtilProvider = Provider<PetUtil>((ref) => PetUtil());

class PetUtil {
  late Map _shelterMap;

  PetUtil() {
    _shelterMap = Map<int, String>();
    _shelterMap[-1] = '*全部收容所';
    _shelterMap[48] = '基隆市寵物銀行';
    _shelterMap[49] = '臺北市動物之家';
    _shelterMap[50] = '新北市板橋區公立動物之家';
    _shelterMap[51] = '新北市新店區公立動物之家';
    _shelterMap[53] = '新北市中和區公立動物之家';
    _shelterMap[55] = '新北市淡水區公立動物之家';
    _shelterMap[56] = '新北市瑞芳區公立動物之家';
    _shelterMap[58] = '新北市五股區公立動物之家';
    _shelterMap[59] = '新北市八里區公立動物之家';
    _shelterMap[60] = '新北市三芝區公立動物之家';
    _shelterMap[61] = '桃園市動物保護教育園區';
    _shelterMap[62] = '新竹市動物收容所';
    _shelterMap[63] = '新竹縣動物收容所';
    _shelterMap[67] = '臺中市動物之家南屯園區';
    _shelterMap[68] = '臺中市動物之家后里園區';
    _shelterMap[69] = '彰化縣流浪狗中途之家';
    _shelterMap[70] = '南投縣公立動物收容所';
    _shelterMap[71] = '嘉義市流浪犬收容中心';
    _shelterMap[72] = '嘉義縣流浪犬中途之家';
    _shelterMap[73] = '臺南市動物之家灣裡站';
    _shelterMap[74] = '臺南市動物之家善化站';
    _shelterMap[75] = '高雄市壽山動物保護教育園區';
    _shelterMap[76] = '高雄市燕巢動物保護關愛園區';
    _shelterMap[77] = '屏東縣流浪動物收容所';
    _shelterMap[78] = '宜蘭縣流浪動物中途之家';
    _shelterMap[79] = '花蓮縣流浪犬中途之家';
    _shelterMap[80] = '臺東縣動物收容中心';
    _shelterMap[81] = '連江縣流浪犬收容中心';
    _shelterMap[82] = '金門縣動物收容中心';
    _shelterMap[83] = '澎湖縣流浪動物收容中心';
    _shelterMap[89] = '雲林縣流浪動物收容所';
    _shelterMap[92] = '新北市政府動物保護防疫處';
    _shelterMap[96] = '苗栗縣生態保育教育中心';
//    shelterMap.keys.toList();
  }

  String getKind(String? name) {
    switch (name) {
      case "貓":
        return '貓';
      // break;
      case "狗":
        return '狗';
      case "其他":
        return "其他";
      default:
        return "其他";
    }
  }

  String? getSex(String? name) {
    switch (name) {
      case "M":
        return '公';
      case "F":
        return '母';
      case "N":
        return null;
      default:
        return null;
    }
  }

  String getLocale(int code) {
    switch (code) {
      case 2:
        return '台北市';
      case 3:
        return '新北市';
      case 4:
        return '基隆市';
      case 5:
        return '宜蘭縣';
      case 6:
        return '桃園縣';
      case 7:
        return '新竹縣';
      case 8:
        return '新竹市';
      case 9:
        return '苗栗縣';
      case 10:
        return '台中市';
      case 11:
        return '彰化縣';
      case 12:
        return '南投縣';
      case 13:
        return '雲林縣';
      case 14:
        return '嘉義縣';
      case 15:
        return '嘉義市';
      case 16:
        return '台南市';
      case 17:
        return '高雄市';
      case 18:
        return '屏東縣';
      case 19:
        return '花蓮縣';
      case 20:
        return '台東縣';
      case 21:
        return '澎湖縣';
      case 22:
        return '金門縣';
      case 23:
        return '連江縣';
      default:
        return "";
    }
  }

  String getBody(String? bodyType) {
    switch (bodyType) {
      case 'SMALL':
        return '小型';
      case 'MEDIUM':
        return '中型';
      case 'BIG':
        return '大型';
      default:
        return "";
    }
  }

  String getAge(String? age) {
    switch (age) {
      case 'CHILD':
        return '幼年';
      case 'ADULT':
        return '成年';
      default:
        return "";
    }
  }

  String getColour(String? color) {
    if (color != null && color != '') {
      return color;
    } else {
      return "";
    }
  }

  String getCurrentPlace(String? place) {
    if (place != null && place.isNotEmpty) {
      return place;
    } else {
      return '未輸入';
    }
  }

  String getFoundPlace(String place) {
    if (place != '') {
      return place;
    } else {
      return '未輸入';
    }
  }

  String getRemark(String remark) {
    if (remark != '') {
      return remark;
    } else {
      return '未輸入';
    }
  }

  String getSterilization(String? state) {
    switch (state) {
      case 'T':
        return '已絕育';
      case 'F':
        return '未絕育';
      case 'N':
        return '絕育未知';
      default:
        return "";
    }
  }

  String getBacterin(String state) {
    switch (state) {
      case 'T':
        return '已打疫苗';
      case 'F':
        return '未打疫苗';
      case 'N':
        return '疫苗未知';
      default:
        return "";
    }
  }

  String getShelterNameByPkID(int pkid) {
    return _shelterMap[pkid];
  }

  Map get shelterMap => _shelterMap;

  int getShelterKeyByValue(String value) {
    int key = shelterMap.keys
        .firstWhere((k) => shelterMap[k] == value, orElse: () => null);
    return key;
  }
}
